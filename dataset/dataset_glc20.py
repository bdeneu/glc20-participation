from dataset.rasters_glc2020 import PatchExtractor
from torch.utils.data import Dataset
import os
import torch

import numpy as np

_mapping_ces_bio = {

}

patch_extractor = None


class DatasetGLC20(Dataset):
    def __init__(self, labels, dataset, ids, rasters, patches, one_hot=True, mapping_ces_bio=True, use_rasters=True,
                 use_patches=True, only_im=False, extract_size=256):
        """
        :param labels:
        :param dataset: (latitude, longitude)
        :param ids:
        :param rasters:
        :param patches:
        :param mapping_ces_bio:
        """
        self.mapping_ces_bio = mapping_ces_bio
        self.labels = labels
        self.dataset = dataset
        self.ids = ids

        self.one_hot_size = 34
        self.do_one_hot = one_hot
        self.one_hot = np.eye(self.one_hot_size)

        self.rasters = rasters

        self.patches = patches
        self.use_patches = use_patches
        self.only_im = only_im
        self.extract_size = extract_size
        global patch_extractor
        if patch_extractor is None and rasters is not None and use_rasters:
            # 256 is mandatory as images have been extracted in 256 and will be stacked in the __getitem__ method
            patch_extractor = PatchExtractor(rasters, size=extract_size, verbose=True)
            patch_extractor.add_all()

        self.extractor = patch_extractor

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        latitude = self.dataset[idx][0]
        longitude = self.dataset[idx][1]
        id_ = str(self.ids[idx])

        # folders that contain patches
        folder_1 = id_[-2:]
        folder_2 = id_[-4:-2]

        # path to patches
        path = os.path.join(self.patches, folder_1, folder_2, id_)
        path_alti = path + '_alti.npy'
        path_rgb_ir_lc = path + '.npy'

        # extracting patch from rasters
        tensor = None
        if self.extractor is not None:
            tensor = self.extractor[(latitude, longitude)]

        if not self.use_patches:
            return torch.from_numpy(tensor).float(), self.labels[idx]

        try:
            # extracting altitude patch
            if not self.only_im:
                alti = np.load(path_alti)
            # extracting rgb infra-red and land cover (5, 256, 256)
            rgb_ir_lc = np.load(path_rgb_ir_lc).transpose((2, 0, 1))
            
            if not self.only_im:
                # transforming landcover in one hot encoding
                if self.do_one_hot:
                    lc = rgb_ir_lc[4]
                    lc_one_hot = np.zeros((self.one_hot_size, lc.shape[0], lc.shape[1]))
                    row_idx = np.arange(lc.shape[0]).reshape(lc.shape[0], 1)
                    col_idx = np.tile(np.arange(lc.shape[1]), (lc.shape[0], 1))
                    lc_one_hot[lc, row_idx, col_idx] = 1
                else:
                    lc_one_hot = lc = rgb_ir_lc[4:5]

            # concatenating all patches
            if self.only_im:
                tensor = rgb_ir_lc[:4]
            elif tensor is not None:
                tensor = np.concatenate((tensor,
                                         alti.reshape((1, alti.shape[0], alti.shape[1])),
                                         rgb_ir_lc[:4],
                                         lc_one_hot))
            else:
                tensor = np.concatenate((alti.reshape((1, alti.shape[0], alti.shape[1])),
                                         rgb_ir_lc[:4],
                                         lc_one_hot))
        except Exception:
            tensor = np.zeros((1, self.extract_size, self.extract_size))
            print('{} - {}'.format(path_rgb_ir_lc, path_alti))
        return torch.from_numpy(tensor).float(), self.labels[idx]

    def numpy(self):
        """
        :return: a numpy dataset of 1D vectors
        """
        return np.array([torch.flatten(self[i][0]).numpy() for i in range(len(self))])

    def analyse_patchs(self):
        for i in range(len(self)):
            p = self[i]